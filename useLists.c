// useLists.c
// Author: Joseph Harris
// Date: 20/5/14
// Tutor: Sarah Bennett
// The front end of the linkedlists ADT, can do all kinds
// of linkedlists operations using only the interface provided
// in linkedlists.h
// Feel free to change the code in main to use the interface functions.
// Mess around and see what you can learn from it.

#include <stdlib.h>
#include <stdio.h>

#include "linkedlists.h"

int main (int argc, char *argv[]) {
    List myList = newList (10); 
    append (myList, 30);
    append (myList, 24);
    append (myList, 130);
    append (myList, (int) 'a');
    //insert (myList, 2, -10);
    removeNode (myList, 2);
    pop (myList);
    //removeNode (myList, 2);
    //assignValue (myList, 2, 15);
    //removeNode (myList, 0);
    int i = 0;
    // Prints out the list.
    while (i < getLength (myList)) {
    	printf ("Value at index %d is: %d\n", i, valueAtIndex (myList, i));
    	i++;
    }
    
	return EXIT_SUCCESS;
}