#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "list3-extraFns.h"

static void testList (void);

int main (int argc, char *argv[]) {
    testList();
    printf ("All tests passed");
    return EXIT_SUCCESS;
}

list newList (void) {
    list myList = malloc (sizeof (*myList));
    myList->head = NULL;
    return myList;
}

// delete the first node
void deleteHead (list l) {
	if (l->head != NULL) {
		l->head = l->head->next;		
	}
}

// delete the end node
void deleteLast (list l) {
	// if only one node left
	if (l->head->next == NULL) {
		//printf ("Are we here");
        l->head = NULL;
    // if there is more than one node left
	} else if (l->head != NULL) {
        link current = l->head;
        link previous = l->head;
        while (current->next != NULL) {
        	previous = current;
        	current = current->next;
        }
        previous->next = NULL;
	}
}

// join two lists

// move all the elements from the second list to the end of
// the first list (in order).  this leaves the second list
// empty and all the elements in the first list. 
void concatenate (list to, list from) {
	link current = to->head;
	if (current != NULL) {
        while (current->next != NULL) {
			current = current->next;
		}
		// doesn't matter if 'from' is empty
		current->next = from->head;
	} else {
		to->head = from->head;
	} 
	from->head = NULL;
	
}

// count the number of nodes containing 42
int count42s (list l) {
	int count = 0;
	link current = l->head;
	while (current != NULL) {
		if (current->value == 42) {
			count++;
		}
		current = current->next;
	}
    return count;
}

static void testList (void) {
    printf ("testing newList()\n");
    list testList = newList();
    assert (testList->head == NULL);
 
 
    printf ("testing deleteHead()\n");
    // create a 3 element list
    testList->head = malloc (sizeof (node));
    testList->head->value = 3;
    testList->head->next  = malloc (sizeof (node));
    testList->head->next->value = 1;
    testList->head->next->next  = malloc (sizeof (node));
    testList->head->next->next->value = 4;
    testList->head->next->next->next  = NULL;
 
    deleteHead (testList);
 
    assert (testList != NULL);
    assert (testList->head != NULL);
    assert (testList->head->value == 1);
    assert (testList->head->next != NULL);
    assert (testList->head->next->value == 4);
    assert (testList->head->next->next == NULL);
 
    deleteHead (testList);
 
    assert (testList != NULL);
    assert (testList->head != NULL);
    assert (testList->head->value == 4);
    assert (testList->head->next == NULL);
 
    deleteHead (testList);
 
    assert (testList != NULL);
    assert (testList->head == NULL);
 
 
    printf ("testing deleteLast()\n");
    // create a 3 element list
    testList->head = malloc (sizeof (node));
    testList->head->value = 3;
    testList->head->next  = malloc (sizeof (node));
    testList->head->next->value = 1;
    testList->head->next->next  = malloc (sizeof (node));
    testList->head->next->next->value = 4;
    testList->head->next->next->next  = NULL;
 
    deleteLast (testList);
 
    assert (testList != NULL);
    assert (testList->head != NULL);
    assert (testList->head->value == 3);
    assert (testList->head->next != NULL);
    assert (testList->head->next->value == 1);
    assert (testList->head->next->next == NULL);
 
    deleteLast (testList);
 
    assert (testList != NULL);
    assert (testList->head != NULL);
    assert (testList->head->value == 3);
    assert (testList->head->next == NULL);
 
    deleteLast (testList);
 
    assert (testList != NULL);
    assert (testList->head == NULL);
 
 
    printf ("testing concatenate()\n");
 
    // (3,2) -> (5,0)
    // create a 3 element list
    testList->head = malloc (sizeof (node));
    testList->head->value = 3;
    testList->head->next  = malloc (sizeof (node));
    testList->head->next->value = 1;
    testList->head->next->next  = malloc (sizeof (node));
    testList->head->next->next->value = 4;
    testList->head->next->next->next  = NULL;
 
    // create a two element list
    list testListB = newList();
 
    testListB->head = malloc (sizeof (node));
    testListB->head->value = 1;
    testListB->head->next  = malloc (sizeof (node));
    testListB->head->next->value = 5;
    testListB->head->next->next  = NULL;
 
    concatenate (testList, testListB);
    assert (testListB != NULL);
    assert (testListB->head == NULL);
 
    assert (testList != NULL);
    assert (testList->head != NULL);
    assert (testList->head->value == 3);
    assert (testList->head->next != NULL);
    assert (testList->head->next->value == 1);
    assert (testList->head->next->next != NULL);
    assert (testList->head->next->next->value == 4);
    assert (testList->head->next->next->next != NULL);
    assert (testList->head->next->next->next->value == 1);
    assert (testList->head->next->next->next->next != NULL);
    assert (testList->head->next->next->next->next->value == 5);
    assert (testList->head->next->next->next->next->next == NULL);
 
 
    // (5,0) -> (5,0)
    concatenate (testList, testListB);
    assert (testListB != NULL);
    assert (testListB->head == NULL);
 
    assert (testList != NULL);
    assert (testList->head != NULL);
    assert (testList->head->value == 3);
    assert (testList->head->next != NULL);
    assert (testList->head->next->value == 1);
    assert (testList->head->next->next != NULL);
    assert (testList->head->next->next->value == 4);
    assert (testList->head->next->next->next != NULL);
    assert (testList->head->next->next->next->value == 1);
    assert (testList->head->next->next->next->next != NULL);
    assert (testList->head->next->next->next->next->value == 5);
    assert (testList->head->next->next->next->next->next == NULL);
 
    // (0,5) -> (5,0)
    concatenate (testListB, testList);
    assert (testList != NULL);
    assert (testList->head == NULL);
 
    assert (testListB != NULL);
    assert (testListB->head != NULL);
    assert (testListB->head->value == 3);
    assert (testListB->head->next != NULL);
    assert (testListB->head->next->value == 1);
    assert (testListB->head->next->next != NULL);
    assert (testListB->head->next->next->value == 4);
    assert (testListB->head->next->next->next != NULL);
    assert (testListB->head->next->next->next->value == 1);
    assert (testListB->head->next->next->next->next != NULL);
    assert (testListB->head->next->next->next->next->value == 5);
    assert (testListB->head->next->next->next->next->next == NULL);
 
    printf ("testing count42s()\n");
    assert (count42s (testList) == 0);
    assert (count42s (testListB) == 0);
 
    testListB->head->value = 42;
    assert (count42s (testListB) == 1);
 
    testListB->head->next->value = 42;
    assert (count42s (testListB) == 2);
 
    testListB->head->next->next->value = 42;
    assert (count42s (testListB) == 3);
 
    testListB->head->next->next->next->value = 42;
    assert (count42s (testListB) == 4);
 
    testListB->head->next->next->next->next->value = 42;
    assert (count42s (testListB) == 5);
 
 
}