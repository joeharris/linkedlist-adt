// linkedlists.c
// Author: Joseph Harris
// Date: 20/5/14
// Tutor: Sarah Bennett
// The backend code for using linkedlists

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "linkedlists.h"

// Necessary setup, typedef the node struct itself,
// and a special struct for the head of the linkedlist.

typedef struct _node {
    int value;
    Node next;
} node;

typedef struct _head {
    Node first;
} head;

// Creates a new list with one element in it.
// To have a function that just creates an empty list
// do not add the makeNode line.
List newList (int firstValue) {
    // Mallocs space for this new list head
    List myList = malloc (sizeof (head));
    // Adds first element to the list (first is often also called head)
    myList->first = makeNode (firstValue);
    return myList;
}

// Inserts a node at a given index
void insert (List myList, int index, int value) {
	assert (index <= getLength (myList));
	// Start at the first node.
    Node current = myList->first;
    int i = 0;
    // While required index not reached scroll through list.
    while (i < index - 1) {
    	current = current->next;
    	i++;
    }
    // Creates a new node, with value 'value' then points this new node
    // to the next node in the list, then sets the node before to point
    // to this new node.
    Node newNode = makeNode (value);
    // Special case, if want to add one to very front of list (new 'first')
    // then change myList->first around to have this new node there.
    // Otherwise just do as described in comment above.
    if (index == 0) {
    	newNode->next = myList->first;
    	myList->first = newNode;
    } else {
        newNode->next = current->next;
        current->next = newNode;
    }
}

// Removes the node at the given index.
void removeNode (List myList, int index) {
    // Start at the first node.
    Node current = myList->first;
    int i = 0;
    // While required index not reached scroll through list.
    // it is minus one to accomodate the fact the list starts with
    // one element in it already, the myList->first element.
    while (i < index - 1) {
    	current = current->next;
    	i++;
    }
    // Temporarily saves the node to remove as toRemove, then sets
    // current node to point to the node after the one we want to 
    // remove. Then sets the node to remove to point to NULL.
    Node toRemove;
    // Special case, want to remove first node.
    if (index == 0) {
    	toRemove = myList->first;
    	myList->first = myList->first->next;
    // Do as described above.
    } else {
    	toRemove = current->next;
    	current->next = current->next->next;
    }
    
    destroy (toRemove);
}

// Returns the value at the index given
int valueAtIndex (List myList, int index) {
	int value;
	// Start at the first node.
    Node current = myList->first;
    int i = 0;
    // While required index not reached scroll through list.
    while (i < index) {
    	current = current->next;
    	i++;
    }
    // get the value from the node
    value = current->value;
    return value;
}

// Assigns a value to an existing node
void assignValue (List myList, int index, int value) {
	// Start at the first node.
    Node current = myList->first;
    int i = 0;
    // While required index not reached scroll through list.
    while (i < index) {
    	current = current->next;
    	i++;
    }
    // Sets value of required node to requested value.
    current->value = value;
}

// Appends a node to the end of the list
void append (List myList, int value) {
    // Scrolls through the list to the end
    Node current = myList->first;
    while (current->next != NULL) {
    	current = current->next;
    }
    // Creates a new node that by default points to null
    // make this the new last node.
    Node newNode = makeNode (value);
    current->next = newNode;
}

// Removes the last element
void pop (List myList) {
    // Scrolls to the second last element.
    // 01->02->X, finds 01 as the ->next->next value is a null pointer.
	Node current = myList->first;
    while (current->next->next != NULL) {
    	current = current->next;
    }
    // Temporarily saves node to remove as toRemove, then sets the 2nd 
    // last node to point to NULL, then frees the node to remove
    Node toRemove = current->next;
    current->next = NULL;
    destroy (toRemove);

}
// Does the legwork to produce a new node, mallocs the memory,
// assigns the initial value, points it to NULL.
Node makeNode (int value) {
    Node newNode = malloc (sizeof (node));
    newNode->value = value;
    newNode->next = NULL;
    return newNode;
}

// Returns number of nodes in the list (including first)
int getLength (List myList) {
	int length = 1;
    // Scrolls to the very end, until it is actually a NULL
    // element, this is different to normal, where we use a
    // current->next != NULL condition. This is because we want 
    // to purely count the elements, when you want to do things
    // to the last element you do current->next as that will stop
    // just before following that last NULL pointer to oblivion.
	Node current = myList->first;
	while (current != NULL) {
		length++;
		current = current->next;
	}
	return length;
}

// Just frees the Node instance called node (it is a pointer to
// the malloced memory I created in makeNode!)
void destroy (Node node) {
	free (node);
}