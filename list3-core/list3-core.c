// Study for the linkedlists prac test tomorrow
// Author: Joe Harris
// Date: 25/5/14

#include <stdlib.h>
#include <stdio.h>
#include "list3-core.h"


// print out a list
void showList (list listToPrint) {
    link current = listToPrint->head;
    int counter = 0;
    while (current != NULL) {
    	printf ("Index %d, value: %d\n", counter, lookup (listToPrint, counter));
    	current = current->next;
    	counter++;
    }
}

// inset item at the front of the list
void frontInsert (list l, int item) {
    link newNode = malloc (sizeof (node));
    newNode->value = item;
    newNode->next = l->head;
    l->head = newNode;
}

// count the number of items in the list
int numItems (list l) {
    link current = l->head;
    int length = 0;
    while (current != NULL) {
        length++;
        current = current->next;
    }
    return length;
}

// insert at end of list
void append (list l, int value) {
    link current = l->head;
    link newNode = malloc(sizeof(node));
    newNode->value = value;
    newNode->next = NULL;
    if (current != NULL) {
	    while (current->next != NULL) {
	    	current = current->next;
	    }
	    current->next = newNode;
	} else {
		l->head = newNode;
	}

}

// find the value stored at position i
// i MUST be a valid position in the list
// dont call this on positions outside the list
int lookup (list l, int position) {
    link current = l->head;
    int value;
    int cPos = 0;
    while (cPos < position) {
        current = current->next;
        cPos++;
    }
    value = current->value;
    return value;
}