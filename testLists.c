// testLists.c
// Author: Joseph Harris
// Date: 20/5/14
// Tutor: Sarah Bennett
// The tests for the interface functions of the linklists ADT

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "linkedlists.h"

#define FIRST_VALUE 		10
#define SECOND_VALUE 		20

void testNewList (void);
void testInsert (void);
void testRemoveNode (void);
void testValueAtIndex (void);
void testAssignValue (void);
void testAppend (void);
void testPop (void);
void testMakeNode (void);
void testGetLength (void);
void testDestroy (void);

int main (int argc, char *argv[]){
	testNewList ();
	testInsert ();
	testRemoveNode ();
	testValueAtIndex ();
	testAssignValue ();
	testAppend ();
	testPop ();
	testMakeNode ();
	testGetLength ();
	testDestroy ();
	printf ("All tests passed! You're awesome!\n");
    return EXIT_SUCCESS;
}

void testNewList (void) {
    List testList = newList (FIRST_VALUE);
    // asserts a list has been created at all
    assert (testList != NULL);
    // asserts that the first value is what is assigned
    assert (valueAtIndex (testList, 0) == FIRST_VALUE);
    free (testList);
}

void testInsert (void) {
	List testList = newList (FIRST_VALUE);
	append (testList, 30);
	append (testList, 30);
	int initialLength = getLength (testList);
	// insert node
	insert (testList, 1, 20);
	// check it has been inserted
	assert (valueAtIndex (testList, 1) == 20);
	assert (getLength (testList) == initialLength + 1);
    free (testList);
}

void testRemoveNode (void) {
	List testList = newList (FIRST_VALUE);
	append (testList, 30);
	append (testList, 25);
	append (testList, 20);
	append (testList, 15);
	append (testList, 10);
	append (testList, 5);
	int initialLength = getLength (testList);
	removeNode (testList, 2);
	assert (getLength (testList) == initialLength - 1);
	//printf ("%d value at index 2", valueAtIndex (testList, 2));
	assert (valueAtIndex (testList, 2) == 20);
	free (testList);
}

void testValueAtIndex (void) {
	List testList = newList (FIRST_VALUE);
	// populates a testing list, then asserts values are where they're
	// supposed to be.
	append (testList, 30);
	append (testList, 25);
	append (testList, 20);
	append (testList, 15);
	append (testList, 10);
	append (testList, 5);
	assert (valueAtIndex (testList, 2) == 25);
	assert (valueAtIndex (testList, 1) == 30);
	assert (valueAtIndex (testList, 0) == FIRST_VALUE);
	assert (valueAtIndex (testList, 3) == 20);
	assert (valueAtIndex (testList, 4) == 15);
	assert (valueAtIndex (testList, 6) == 5);
	assert (valueAtIndex (testList, 5) == 10);
}

void testAssignValue (void) {
	List testList = newList (FIRST_VALUE);
	append (testList, 30);
	//assert (valueAtIndex (testList, 1) == 30);
	// assigns a value and tests if correctly assigned
	assignValue (testList, 1, 15);
	assert (valueAtIndex (testList, 1) == 15);
}

void testAppend (void) {
	List testList = newList (FIRST_VALUE);
	append (testList, 30);
	append (testList, 25);
	append (testList, 20);
	append (testList, 15);
	append (testList, 10);
	append (testList, 5);
	int initialLength = getLength (testList);
	append (testList, 40);
	assert (getLength (testList) == initialLength + 1);
	assert (valueAtIndex (testList, getLength (testList) - 1) == 40);
}

void testPop (void) {
    List testList = newList (FIRST_VALUE);
	append (testList, 30);
	append (testList, 25);
	append (testList, 20);
	append (testList, 15);
	append (testList, 10);
	append (testList, 5);
	int initialLength = getLength (testList);
	pop (testList);
	assert (getLength (testList) == initialLength - 1);
	assert (valueAtIndex (testList, getLength (testList) -1) == 10);
}

void testMakeNode (void) {
	Node newNode = makeNode (FIRST_VALUE);
	assert (newNode != NULL);
}

void testGetLength (void) {
	List testList = newList (FIRST_VALUE);
	append (testList, 30);
	append (testList, 25);
	append (testList, 20);
	append (testList, 15);
	append (testList, 10);
	append (testList, 5);
	assert (getLength (testList) == 7);
}
void testDestroy (void) {
	// makes a new node and asserts that it exists
	Node newNode = makeNode (FIRST_VALUE);
	assert (newNode != NULL);
    // destroys it and asserts it is gone
    destroy (newNode);
    assert (newNode == NULL);
}
