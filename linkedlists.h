// linkedlists.h
// Author: Joseph Harris
// Date: 20/5/14
// Tutor: Sarah Bennett
// The interface for the linkedlists.c file

// Pointers to structs
typedef struct _node *Node;
typedef struct _head *List;

// Create a new linkedlist with one node
List newList (int firstValue);
// Insert a node at the provided index, providing 0 inserts one straight
// after head.
void insert (List myList, int index, int value);
// Remove a node at the given index
void removeNode (List myList, int index);
// Returns the value at the index given
int valueAtIndex (List myList, int index);
// Assigns a value to an existing node
void assignValue (List myList, int index, int value);
// Appends a node to the end of the list
void append (List myList, int value);
// Pops off the last node of the list
void pop (List myList);
// Does the legwork to produce a new node, mallocs the memory.
Node makeNode (int value);
// Returns the integer length of the list
int getLength (List myList);
// Frees node
void destroy (Node node);
