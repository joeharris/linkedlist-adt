// orderedInsert.c
// 
 
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "list.h"

#define TRUE 1
#define FALSE 0
 
// given a list of nodes which is arranged in non-decreaing order
// eg 1->4->6->10->X
// and a number, create a new node containing the number and 
// insert it in the correct place in the list so that the the
// resultant one-element-longer list is still in non decreasing
// order.  
// insert the new node by rearranging pointers, don't change the 
// value field for nodes already in the list.
// (if the number being inserted is already in the list 
// you may place the new node before or after the existing nodes
// however you wish just so long as the resulting list is in order.)
 
// eg inserting 3 in the list above would produce the list
// 1->3->4->6->10->X

/*int main () {
	list myList = malloc (sizeof (*myList));
	return EXIT_SUCCESS;
}*/

void orderedInsert (list l, int number) { 
	int done = FALSE;
    link newNode = malloc (sizeof (node));
    newNode->value = number;
    link current = l->head;
    // if should go first
    if (l->head == NULL) {
        newNode->next = l->head;
    	l->head = newNode;
    } else if (number < l->head->value) {
    	newNode->next = l->head;
    	l->head = newNode;
    // if empty list
    } else {
        while (current->next != NULL && done == FALSE) {
    		if (current->next->value < number) {
    			current = current->next;
    		} else {
    			done = TRUE;
    		}
    	}
    	newNode->next = current->next;
    	current->next = newNode;
    }
}